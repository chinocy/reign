import React, { useEffect } from "react";
import "./App.css";
import {
  AppBar,
  IconButton,
  Typography,
  Grid,
  Tooltip,
  Snackbar
} from "@material-ui/core";

import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import moment from "moment";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const theme = createMuiTheme({
  typography: {
    fontFamily: '"Roboto"'
  }
});

const useStyles = makeStyles(theme => ({
  header: {
    color: "white",
    background: "#474646",
    padding: "40px 20px"
  },
  author: {
    color: "#999",
    fontSize: 13
  },
  pointer: {
    "&:hover": {
      cursor: "Pointer"
    }
  },
  row: {
    display: "flex",
    alignItems: "center",
    background: "#fff",
    height: 60,
    margin: "auto",
    width: "97%",
    borderBottom: "1px solid #ccc",
    "&:hover": {
      background: "#fafafa",
      cursor: "Pointer"
    }
  },
  story: {
    fontColor: "#333",
    fontSize: 13
  },
  delete: {
    right: 0
  },
  date: {
    fontSize: 13,
    fontColor: "#333"
  }
}));

function App() {
  const classes = useStyles();
  const [posts, setPosts] = React.useState([]);
  const [open, setOpen] = React.useState(false);
  function loadPosts() {
    axios.get(`http://localhost:3000/posts`).then(res => {
      setPosts(res.data);
    });
  }

  useEffect(() => {
    loadPosts();
  }, []);

  const handleClose = () => setOpen(false);
  const openTab = post => {
    let url = post.story_url || post.url;
    if (url) window.open(url.value || url, "_blank");
    else setOpen(true);
  };

  const onDelete = (e, post) => {
    e.stopPropagation();
    axios.delete(`http://localhost:3000/posts/${post._id}`).then(res => {
      loadPosts();
    });
  };

  const printDatetime = post_datetime => {
    let datetime = moment(post_datetime);
    let today = new Date();
    today.setHours(23, 59, 59);
    today = moment(today);
    switch (today.diff(datetime, "days")) {
      case 0:
        return datetime.format("h:mm a");
      case 1:
        return "Yesterday";
      default:
        return datetime.format("MMM DD");
    }
  };

  const forceDataRefresh = () => {
    axios.post(`http://localhost:3000/forceDataRefresh/`).then(res => {
      loadPosts();
    });
  };
  return (
    <MuiThemeProvider theme={theme}>
      <AppBar position="static" className={classes.header}>
        <Typography variant="h2">HN FEED</Typography>
        <Typography variant="h6">We &lt;3 hacker news!</Typography>
        <Typography
          variant="h7"
          className={classes.pointer}
          onClick={() => forceDataRefresh()}
        >
          Force data refresh
        </Typography>
      </AppBar>
      <Snackbar open={open} autoHideDuration={2000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          El Post no tiene URL
        </Alert>
      </Snackbar>
      <div>
        {posts.map(
          post =>
            (post.story_title || post.title) && (
              <Grid
                container
                className={classes.row}
                key={post._id}
                onClick={() => openTab(post)}
              >
                <Grid item xs={8} className={classes.story}>
                  {post.story_title || post.title}{" "}
                  <span className={classes.author}>- {post.author} -</span>
                </Grid>
                <Grid item xs={3} className={classes.date}>
                  {printDatetime(post.created_at)}
                </Grid>
                <Grid item xs={1} className={classes.icon}>
                  <Tooltip title="Delete" placement="left">
                    <IconButton
                      aria-label="delete"
                      size="medium"
                      onClick={e => onDelete(e, post)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </Tooltip>
                </Grid>
              </Grid>
            )
        )}
      </div>
    </MuiThemeProvider>
  );
}

export default App;
