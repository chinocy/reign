# Reign Project

Build and run image with:

`docker-compose up --build`

Information

- There is a link in the header to forcing data refresh.
- Posts are not physically deleted, they are marked with a deleted mark, ensuring that they do not appear again in the future.
- I'm assume datetime come in UTC.
- Datetimes are shown in local timezone.
- Automatic task run every hour at minute 0 to get Posts.
