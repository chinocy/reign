const express = require("express");
const app = express();
var cron = require("node-cron");
var https = require("https");
const MongoClient = require("mongodb").MongoClient;
const url = "mongodb://mongo:27017/";
const dbName = "Reign";
const collName = "Posts";
const apiUrl = "https://hn.algolia.com/api/v1/search_by_date?query=nodejs";

function loadData() {
  return new Promise((resolve, reject) => {
    MongoClient.connect(url, function(err, client) {
      if (err) reject(err.errmsg);
      else {
        var db = client.db(dbName);
        https
          .get(apiUrl, resp => {
            let data = "";
            resp.on("data", chunk => {
              data += chunk;
            });
            resp.on("end", () => {
              let hits = JSON.parse(data).hits;
              hits.forEach(hit => {
                hit._id = hit.objectID;
                hit.deleted = false;
              });
              db.collection(collName).insertMany(
                hits,
                { ordered: false },
                function(err, res) {
                  if (err) {
                    console.log(err.writeErrors);
                    resolve("resolved");
                  } else {
                    console.log("Post inserted: " + res.insertedCount);
                    console.log(
                      "Inserted ids: " + JSON.stringify(res.insertedIds)
                    );
                    client.close();
                    resolve("resolved");
                  }
                }
              );
            });
          })
          .on("error", e => {
            console.error(e);
            reject(e);
          });
      }
    });
  });
}

// Create database
MongoClient.connect(url + "/" + dbName, function(err, client) {
  if (err) console.log(err);
  else {
    console.log("Database " + dbName + " ready!");
    client.close();
  }
});

// Create collection
MongoClient.connect(url, function(err, client) {
  if (err) {
    console.log(err);
  } else {
    var db = client.db(dbName);
    db.createCollection(collName, function(err, res) {
      if (err) {
        console.log(err.errmsg);
      } else {
        console.log("Collection " + collName + " ready!");
        client.close();
      }
    });
  }
});

cron.schedule("0 * * * *", () => {
  console.log("Running task every hour at minute 0 : " + new Date().toString());
  loadData();
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.get("/posts", function(req, res) {
  MongoClient.connect(url, function(err, client) {
    if (err) console.log(err.errmsg);
    else {
      var db = client.db(dbName);
      db.collection(collName)
        .find({ deleted: false })
        .sort({ created_at: -1 })
        .toArray(function(err, result) {
          if (err) console.log(err.errmsg);
          else {
            client.close();
            res.send(result);
          }
        });
    }
  });
});

app.post("/forceDataRefresh", function(req, res) {
  loadData()
    .then(data => res.send(data))
    .catch(err => res.send(err));
});

app.delete("/posts/:id", function(req, res) {
  MongoClient.connect(url, function(err, client) {
    if (err) throw err;
    var db = client.db(dbName);
    db.collection(collName).updateOne(
      { _id: req.params.id, deleted: false },
      { $set: { deleted: true } },
      function(err, response) {
        if (err) console.log(err.errmsg);
        else {
          console.log(response);
          client.close();
          if (response.modifiedCount)
            res.send("Post " + req.params.id + " deleted.");
          else res.send("No se encontro el post.");
        }
      }
    );
  });
});

app.listen(3000, () => {
  console.log("El servidor está inicializado en el puerto 3000");
});
